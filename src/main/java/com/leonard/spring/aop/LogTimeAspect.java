package com.leonard.spring.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

@Aspect
@Component
@Slf4j
public class LogTimeAspect {

	@Around("@annotation(com.leonard.spring.annotation.LogTime)")
	public Object logTime(ProceedingJoinPoint joinPoint) throws Throwable {
		final StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		var proceed = joinPoint.proceed();
		stopWatch.stop();
		log.debug("(logTime)signature: {}, time: {}ms", joinPoint.getSignature(), stopWatch.getTotalTimeMillis());
		return proceed;
	}
}
