package com.leonard.spring.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

@Aspect
//@Component
@Slf4j
public class RestLogAspect {

//	@Pointcut("execution(public * *(..))")
//	private void publicOperation() {}

	@Pointcut("within(com.leonard.spring.controller.*)")
	public void controllerMethods() {}

//	@Pointcut("publicOperation() && controllerMethods()")
//	public void controllerPublicMethods() {}

	@Before("controllerMethods()")
	public void logBefore(JoinPoint joinPoint, HttpServletRequest request) throws IOException {
		log.info("Class name: {}", joinPoint.getSignature().getDeclaringTypeName());
		log.info("Method: {}", joinPoint.getSignature().getName());
		log.info("Args: {}", Arrays.toString(joinPoint.getArgs()));
		log.info("Target class: {}", joinPoint.getTarget().getClass().getName());

		if (Objects.nonNull(request)) {
			log.info("======================== HTTP REQUEST ================================");
			log.info("Method: {}", request.getMethod());
			log.info("Path: {}", request.getServletPath());

			var headerNames = request.getHeaderNames();
			log.info("Headers:");
			while (headerNames.hasMoreElements()) {
				String headerName = headerNames.nextElement();
				String headerValue = request.getHeader(headerName);
				log.info("{}: {}", headerName, headerValue);
			}

			if (Objects.nonNull(request.getReader())) {
				log.info("Request body: {}", request.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
			}
		}
	}

}
