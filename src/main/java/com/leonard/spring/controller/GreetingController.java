package com.leonard.spring.controller;

import com.leonard.spring.annotation.LogTime;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/v1/greetings")
@RestController
@Slf4j
public class GreetingController {

	@LogTime
	@GetMapping("/hello")
	@ResponseStatus(HttpStatus.OK)
	public String sayHello(@RequestParam String name) {
		log.info("(sayHello) name: {}", name);
		return "Hello " + name;
	}
}